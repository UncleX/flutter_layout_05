import 'package:flutter/material.dart';


void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    //注意这里是:MaterialApp ,不是Material
    return  MaterialApp(
      title: 'demo',
      //方式一
//      home: FirstPage(),
    //方式二
      //When using initialRoute, be sure you do not define a home property
//      //通过路由名称跳转
      initialRoute: '/',
      routes: {
        '/':(context)=>FirstPage(),
        '/second':(context)=>SecondPage(),
        '/thirdPage':(context)=>ThirdController(),
      },
    );
  }
}





//定义第一个路由
class FirstPage extends StatefulWidget {
  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {

  //跳转方法里面实现路由跳转
  void _jump() {
    print('jump to next page');
    //方式一
//    Navigator.push(
//        context, MaterialPageRoute(builder: (context) => SecondPage()));
  Navigator.pushNamed(context, '/second');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('firstPage'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              onPressed: _jump,
              child: Text('跳转到secondPage'),
              color: Colors.blue,
            ),
            RaisedButton(
              onPressed: (){
                Navigator.pushNamed(context, '/thirdPage');
              },
              child: Text('跳转到ThirdPage'),
              color: Colors.blue,
            ),
          ],
        )
      ),
      backgroundColor: Colors.grey,
    );
  }
}


//定义第二个路由
class SecondPage extends StatefulWidget {
  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {

  //跳转方法里面实现路由返回
  void _goBack() {
    print('go back');
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('secondPage'),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: _goBack,
          child: Text('返回'),
          color: Colors.blue,
        ),
      ),
      backgroundColor: Colors.grey,
    );
  }
}

//定义第三个路由
class ThirdController extends StatelessWidget {
  //返回


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('thirdPage'),
      ),
      body: Center(
        child: RaisedButton(onPressed:() {
        Navigator.pop(context);
        },
          child: Text('第三个页面'),
        ),

      ),
    );
  }
}

